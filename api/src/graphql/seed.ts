import { Context } from "./context";

export function Seed(ctx: Context) {
    // Seed the database
    // Create 3 channels: #General, #Random, #Test
    // Create user: @admin
    // Let @admin join #General en #Random
    // Let @admin leave #Test
    // Let @admin post messages in all channels

    const c_general = ctx.channelRepository.add({ name: "General" });
    const c_random = ctx.channelRepository.add({ name: "Random" });
    const c_test = ctx.channelRepository.add({ name: "Test" });

    const u_admin = ctx.userRepository.add({ name: "admin" });

    // Admin joins two channels (but leaves one)
    ctx.linkUserChannelRepository.add({ userId: u_admin.id, active: true, channelId: c_general.id });
    ctx.linkUserChannelRepository.add({ userId: u_admin.id, active: true, channelId: c_random.id });
    ctx.linkUserChannelRepository.add({ userId: u_admin.id, active: false, channelId: c_test.id });

    ctx.messageRepository.add({
        channelId: c_general.id,
        userId: u_admin.id,
        timestamp: new Date(2018, 12, 1, 9, 0),
        body: "First!"
    });

    ctx.messageRepository.add({
        channelId: c_general.id,
        userId: u_admin.id,
        timestamp: new Date(2018, 12, 1, 9, 1),
        body: "And alone after a minute"
    });


    ctx.messageRepository.add({
        channelId: c_random.id,
        userId: u_admin.id,
        timestamp: new Date(),
        body: "I'm so random... 🤪"
    });

    ctx.messageRepository.add({
        channelId: c_test.id,
        userId: u_admin.id,
        timestamp: new Date(2018, 12, 1, 10, 1),
        body: "Leaving this temporary channel... Bye!!"
    })
}
