import { gql } from "apollo-server";

// The GraphQL schema in string form
export const typeDefs = gql`
  scalar Date

  type Query { 
      system: System!

      channels: [Channel]
      channel(id: ID!): Channel

      users: [User]
      user(id: ID!): User

      messages: [Message]
      message(id: ID!): Message
  }

  type Mutation {
    createUser(name: String!): User!
    createChannel(name: String!): Channel!

    joinChannel(userId: ID!, channelId: ID!): LinkUserChannel!
    leaveChannel(userId: ID!, channelId: ID!): LinkUserChannel

    post(userId: ID!, channelId: ID!, body: String!): Message!
  }

  type System {
    timestamp: Date!
    isOnline: Boolean!
  }

  type User {
    id: ID!
    name: String!

    messages: [Message]
    channels(active: Boolean): [Channel]
  }

  type Channel {
    id: ID!
    name: String!

    messages: [Message]
    userLinks(areActive: Boolean): [LinkUserChannel]
  }

  type Message {
    id: ID!
    userId: ID!
    channelId: ID!

    body: String!
    timestamp: Date!

    user: User!
    channel: Channel!

    "An extreme slow method"
    username: String!
    channelname: String!
    singleline: String!
  }

  type LinkUserChannel {
    id: ID!
    userId: ID!
    channelId: ID!

    active: Boolean!

    channel: Channel!
    user: User!
  }
`;