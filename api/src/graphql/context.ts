import {
    Channel,
    LinkUserChannel,
    Message,
    User,
} from "../entities";
import { Repository } from "../repositories/repository.interface";

export interface Context {
    channelRepository: Repository<Channel>;
    linkUserChannelRepository: Repository<LinkUserChannel>;
    messageRepository: Repository<Message>;
    userRepository: Repository<User>;
}