import { Context } from "../../context";
import { Channel } from "../../../entities";

export async function resolve_channels(
    source,
    args,
    context: Context,
    info
): Promise<Channel[]> {
    return context.channelRepository.where(() => true);
}