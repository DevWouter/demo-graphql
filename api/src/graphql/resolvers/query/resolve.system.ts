import { System } from  "../../../entities/system";
import { Context } from "../../context";

export async function resolve_system(
    source,
    args,
    context: Context,
    info
): Promise<System> {
    return Promise.resolve(<System>{
        isOnline: true,
        timestamp: new Date(),
    });
}