import { Context } from "../../context";
import { User } from "../../../entities";

export async function resolve_user(
    source,
    { id }: { id: number },
    context: Context,
    info
): Promise<User> {
    return context.userRepository.byId(id);
}