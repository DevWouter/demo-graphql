import { resolve_channels } from "./resolve.channels";
import { resolve_channel } from "./resolve.channel";
import { resolve_system } from "./resolve.system";
import { resolve_user } from "./resolve.user";
import { resolve_users } from "./resolve.users";
import { resolve_message } from "./resolve.message";
import { resolve_messages } from "./resolve.messages";

export const Query = {
    system: resolve_system,
    channel: resolve_channel,
    channels: resolve_channels,
    user: resolve_user,
    users: resolve_users,
    message: resolve_message,
    messages: resolve_messages,
};