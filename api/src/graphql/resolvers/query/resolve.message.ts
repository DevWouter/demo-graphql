import { Context } from "../../context";
import { Message } from "../../../entities";

export async function resolve_message(
    source,
    { id }: { id: number },
    context: Context,
    info
): Promise<Message> {
    return context.messageRepository.byId(id);
}