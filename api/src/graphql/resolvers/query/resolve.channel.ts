import { Context } from "../../context";
import { Channel } from "../../../entities";

export async function resolve_channel(
    source,
    { id }: { id: number },
    context: Context,
    info
): Promise<Channel> {
    return context.channelRepository.byId(id);
}