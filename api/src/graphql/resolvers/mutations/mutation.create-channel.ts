import { Context } from "../../context";
import { Channel } from "../../../entities";

export async function mutation_createChannel(
    source,
    { name }: { name: string },
    context: Context,
    info
): Promise<Channel> {
    return context.channelRepository.add({ name });
}