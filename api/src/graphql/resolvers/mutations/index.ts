import { mutation_createUser } from "./mutation.create-user";
import { mutation_createChannel } from "./mutation.create-channel";
import { mutation_joinChannel } from "./mutation.join-channel";
import { mutation_leaveChannel } from "./mutation.leave_channel";
import { mutation_post } from "./mutation.post";

export const Mutation = {
    createUser: mutation_createUser,
    createChannel: mutation_createChannel,
    joinChannel: mutation_joinChannel,
    leaveChannel: mutation_leaveChannel,
    post: mutation_post,
}