import { Context } from "../../context";
import { User, Message, LinkUserChannel } from "../../../entities";

export async function mutation_leaveChannel(
    source,
    { userId, channelId }: { userId: number, channelId: number },
    context: Context,
    info
): Promise<LinkUserChannel> {
    if (!context.userRepository.byId(userId)) {
        throw new Error(`User with id ${userId} doesn't exists`)
    }

    if (!context.channelRepository.byId(channelId)) {
        throw new Error(`Channel with id ${channelId} doesn't exists`)
    }

    var links = context.linkUserChannelRepository
        .where(x => x.channelId == channelId && x.userId == userId);

    if (links.length === 0) {
        throw new Error(`User ${userId} has never joined channel ${channelId}`);
    }

    if (links[0].active == false) {
        throw new Error(`User ${userId} has already left channel ${channelId}`);
    }

    return context.linkUserChannelRepository.update(
        links[0].id,
        { active: false });
}