import { Context } from "../../context";
import { User, Message } from "../../../entities";

export async function mutation_post(
    source,
    { userId, channelId, body }: { userId: number, channelId: number, body: string },
    context: Context,
    info
): Promise<Message> {
    if (!context.userRepository.byId(userId)) {
        throw new Error(`User with id ${userId} doesn't exists`)
    }

    if (!context.channelRepository.byId(channelId)) {
        throw new Error(`Channel with id ${channelId} doesn't exists`)
    }

    var links = context.linkUserChannelRepository
        .where(x => x.channelId == channelId && x.userId == userId);

    if (links.length === 0) {
        throw new Error(`Unable to post since user ${userId} has never joined channel ${channelId}`);
    }

    if (links[0].active == false) {
        throw new Error(`Unable to post since user ${userId} has left channel ${channelId}`);
    }

    return context.messageRepository.add({ body, channelId, userId, timestamp: new Date() });
}