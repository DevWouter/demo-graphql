import { Context } from "../../context";
import { User } from "../../../entities";

export async function mutation_createUser(
    source,
    { name }: { name: string },
    context: Context,
    info
): Promise<User> {
    return context.userRepository.add({ name });
}