import { Context } from "../../context";
import { Message, Channel } from "../../../entities";

export async function resolve_message_channel(
    source: Message,
    { id }: { id: number },
    context: Context,
    info
): Promise<Channel> {
    return context.channelRepository.byId(source.channelId);
}