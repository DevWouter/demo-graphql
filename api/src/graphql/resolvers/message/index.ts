import { resolve_message_channel } from "./resolve.message_channel";
import { resolve_message_user } from "./resolve.message_user";
import { resolve_message_username } from "./resolve.message_username";
import { Context } from "../../context";
import { Message } from "../../../entities";

export const MessageResolver = {
    channel: resolve_message_channel,
    user: resolve_message_user,
    username: resolve_message_username,
    channelname: (
        source: Message,
        args,
        context: Context,
        info) => context
            .channelRepository
            .byId(source.channelId)
            .name,
    singleline: (
        source: Message,
        args,
        context: Context,
        info) => {
        const channelName = context
            .channelRepository
            .byId(source.channelId)
            .name;
        const userName = context
            .userRepository
            .byId(source.userId)
            .name;
        return `${userName} in ${channelName}: ${source.body}`
    }
}