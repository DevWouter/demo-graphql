import { Context } from "../../context";
import { Message, Channel, User } from "../../../entities";

let nextCounter = 1;
export async function resolve_message_username(
    source: Message,
    { id }: { id: number },
    context: Context,
    info
): Promise<string> {
    const counter = nextCounter++;
    return new Promise<string>((resolve, reject) => {
        setTimeout(() => {
            resolve(context.userRepository.byId(source.userId).name);
        }, (50 * (counter % 10)));
    });
}