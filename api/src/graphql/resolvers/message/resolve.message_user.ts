import { Context } from "../../context";
import { Message, Channel, User } from "../../../entities";

export async function resolve_message_user(
    source: Message,
    { id }: { id: number },
    context: Context,
    info
): Promise<User> {
    return context.userRepository.byId(source.userId);
}