import { Context } from "../../context";
import { Message, Channel, LinkUserChannel, User } from "../../../entities";

export async function resolve_userLink_user(
    source: LinkUserChannel,
    args,
    context: Context,
    info
): Promise<User> {
    return context.userRepository.byId(source.userId);
}