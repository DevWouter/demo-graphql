import { Context } from "../../context";
import { Message, Channel, LinkUserChannel, User } from "../../../entities";

export async function resolve_userLink_channel(
    source: LinkUserChannel,
    args,
    context: Context,
    info
): Promise<Channel> {
    return context.channelRepository.byId(source.channelId);
}