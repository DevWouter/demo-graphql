import { resolve_userLink_channel } from "./resolve.user-link_channel";
import { resolve_userLink_user } from "./resolve.user-link_user";

export const LinkUserChannelResolver = {
    channel: resolve_userLink_channel,
    user: resolve_userLink_user,
}