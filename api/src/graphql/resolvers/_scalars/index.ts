import { scalar_date } from "./scalar.date";

export const Scalars = {
    Date: scalar_date
}
