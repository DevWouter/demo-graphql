import { Context } from "../../context";
import { Message, Channel, LinkUserChannel, User } from "../../../entities";

export async function resolve_user_messages(
    source: User,
    args,
    context: Context,
    info
): Promise<Message[]> {
    return context.messageRepository
        .where(x => x.userId == source.id);
}