import { resolve_user_messages } from "./resolve.user_messages";
import { resolve_user_channels } from "./resolve.user_channels";

export const UserResolver = {
    messages: resolve_user_messages,
    channels: resolve_user_channels,
}