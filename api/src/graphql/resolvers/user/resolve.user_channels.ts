import { Context } from "../../context";
import { Message, Channel, LinkUserChannel, User } from "../../../entities";

export async function resolve_user_channels(
    source: User,
    { active }: { active?: boolean },
    context: Context,
    info
): Promise<Channel[]> {
    if (active === true) {
        return context.linkUserChannelRepository
            .where(x => x.userId == source.id && x.active == true)
            .map(x => x.channelId)
            .map(x => context.channelRepository.byId(x));
    }

    if (active === false) {
        return context.linkUserChannelRepository
            .where(x => x.userId == source.id && x.active == false)
            .map(x => x.channelId)
            .map(x => context.channelRepository.byId(x));
    }

    return context.linkUserChannelRepository
        .where(x => x.userId == source.id)
        .map(x => x.channelId)
        .map(x => context.channelRepository.byId(x));
}