
import { Mutation } from "./mutations";
import { Query } from "./query";
import { ChannelResolvers } from "./channel";
import { LinkUserChannelResolver } from "./link-user-channel";
import { MessageResolver } from "./message";
import { UserResolver } from "./user";
import { Scalars } from "./_scalars";

export const resolvers = {
    Query,
    Mutation,
    ...Scalars,
    Channel: ChannelResolvers,
    LinkUserChannel: LinkUserChannelResolver,
    Message: MessageResolver,
    User: UserResolver,
};
