import { resolve_channel_messages } from "./resolve.channel_messages";
import { resolve_channel_userLinks } from "./resolve.channel_user-links";

export const ChannelResolvers = {
    messages: resolve_channel_messages,
    userLinks: resolve_channel_userLinks,
}