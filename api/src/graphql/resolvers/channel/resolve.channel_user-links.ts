import { Context } from "../../context";
import { Message, Channel, LinkUserChannel } from "../../../entities";

export async function resolve_channel_userLinks(
    source: Channel,
    { areActive }: { areActive?: boolean },
    context: Context,
    info
): Promise<LinkUserChannel[]> {
    if (areActive === true) {
        return context.linkUserChannelRepository
            .where((x) => x.channelId == source.id && x.active);
    } 
    if (areActive === false) {
        return context.linkUserChannelRepository
            .where((x) => x.channelId == source.id && !x.active);
    }

    return context.linkUserChannelRepository
        .where((x) => x.channelId == source.id);
}