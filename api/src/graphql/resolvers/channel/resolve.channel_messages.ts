import { Context } from "../../context";
import { Message, Channel } from "../../../entities";

export async function resolve_channel_messages(
    source: Channel,
    args,
    context: Context,
    info
): Promise<Message[]> {
    return context.messageRepository.where((x) => x.channelId == source.id);
}