import { ApolloServer } from "apollo-server";
import { typeDefs } from "./graphql/graphql-schema"
import { resolvers } from "./graphql/resolvers";
import { Context } from "./graphql/context";
import { Repository } from "./repositories/repository.interface";
import { Channel, Message, LinkUserChannel, User } from "./entities";
import { Seed } from "./graphql/seed";

var context: Context = {
    channelRepository: new Repository<Channel>(),
    messageRepository: new Repository<Message>(),
    linkUserChannelRepository: new Repository<LinkUserChannel>(),
    userRepository: new Repository<User>(),
};

// Seed the context
Seed(context);

// Setup server
const server = new ApolloServer({
    typeDefs,
    resolvers,
    context,
    // tracing: true
});

server.listen({ port: 3000 }).then((serverInfo) => {
    console.log(`🚀  Server ready at ${serverInfo.url}`);
});
