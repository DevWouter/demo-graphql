interface Entity {
    id: number
};

export class Repository<T extends Entity> {
    private nextId = 1;
    private readonly items: T[] = [];

    byId(id: number): T {
        return this.items.find(x => x.id == id);
    }

    where(predicate: (value: T) => boolean): T[] {
        return this.items.filter(x => predicate(x));
    }

    add(entity: Pick<T, Exclude<keyof T, "id">>): T {
        var entityObj = entity as object;
        const hasNoId = entityObj['id'] === undefined || entityObj['id'] === null || entityObj['id'] === 0;
        if (!hasNoId) {
            throw new Error("Entity already has an id and cannot be added");
        }

        if (this.items.indexOf(entity as T) !== -1) {
            throw new Error("Entity is already in the repository");
        }

        (entity as T).id = this.nextId++;

        this.items.push(entity as T);

        return entity as T;
    }

    update(id: number, values: Partial<Pick<T, Exclude<keyof T, "id">>>): T {
        let oldEntity = this.byId(id);
        if (oldEntity === undefined) {
            throw new Error(`Entity does with id ${id} does not exist`);
        }

        const newEntity = {
            ...(oldEntity as object),
            ...(values as object)
        } as T;

        // Replace in storage
        const index = this.items.findIndex(x => x.id === id);
        this.items.splice(index, 1, newEntity);

        return this.byId(id);
    }

    delete(id: number): T {
        const index = this.items.findIndex(x => x.id === id);
        if (index === -1) {
            throw new Error(`Entity does with id ${id} does not exist`);
        }

        return this.items.splice(index, 1)[0];
    }

    count(): number { return this.items.length; }
}