export interface LinkUserChannel {
    id: number;
    channelId: number;
    userId: number;
    active: boolean;
}