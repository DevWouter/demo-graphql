export interface Message {
    id: number;
    userId: number;
    channelId: number;
    timestamp: Date;
    body: string;
}