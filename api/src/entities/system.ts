export interface System {
    readonly isOnline: boolean;
    readonly timestamp: Date | number;
}