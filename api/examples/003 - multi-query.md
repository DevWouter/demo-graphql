# Query definition

```graphql
query{
  channels{
    id
    name
  }
  
  users{
    id
    name
  }
}
```

# Query variables
```json
```