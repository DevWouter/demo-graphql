# Query definition

Also show what happens if you remove `papa` en `mama`.

```graphql
mutation {
  papa: createUser(name: "Jan"){
    id
  }
  mama: createUser(name: "Joke"){
      id
  }
}
```

# Query variables
```json
```