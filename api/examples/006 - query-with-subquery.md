# Query definition

We want to know the active users.

Use channel 3 to show how it behaves differently.

```graphql
query {
  channel(id: 1){
    name
    userLinks(areActive:true){
      user{
        name
      }
    }
  }
}
```

# Query variables
```json
```
