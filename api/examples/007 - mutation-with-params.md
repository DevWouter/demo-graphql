# Query definition

```graphql
query who_is_in_channel_1($channelId: ID!) {
  channel(id: $channelId) {
    userLinks(onlyActive: true) {
      id
    }
  }
}

mutation leave_channel_1($userId: ID!, $channelId: ID!) {
  leaveChannel(userId: $userId, channelId: $channelId) {
    active
  }
}

mutation join_channel_1($userId: ID!, $channelId: ID!) {
  joinChannel(userId: $userId, channelId: $channelId) {
    active
  }
}
```

# Query variables
```json
{
  "userId": 1,
  "channelId":1
}
```