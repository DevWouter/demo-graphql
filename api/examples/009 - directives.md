# Example of how to handle directives

```gql
directive @authorize(
  roles: [String!]
) on FIELD_DEFINITION

type Query { 
  system: System!
}

type System {
  timestamp: Date!
  isOnline: Boolean!
  secretMessage: String! @authorize(roles: ['admin'])
}

type Mutation {
  setSecretMessage(message: String!): System! @authorize(roles: ['admin'])
}
```