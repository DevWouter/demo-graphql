# Query definition

Proof that it process in parallel

```graphql
query are_parallel{
  messages {
    username
  }
}
mutation are_sequential{
	p1: post(userId: 1, channelId: 1, body:"msg1"){
    username
  }
	p2: post(userId: 1, channelId: 1, body:"msg2"){
    username
  }
}

```

# Query variables
```json
```