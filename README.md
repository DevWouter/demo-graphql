# Demo of GraphQL

Within the demo we have simple chat app to demonstrate the advantage of GraphQL.

## Running the demo

You can run the demo locally. This gives you the advantage of making changes 
to the code and automaticaly updating the server.
Or you can run it in docker (without the hot-reload advantage).

### Run locally

```sh
cd api
npm install
npm run start:watch
```

Afterwards the you can go to http://localhost:3000 to see the GraphQL interface.

### Run in docker

```sh
docker-compose up --build
```

Afterwards the you can go to http://localhost:3000 to see the GraphQL interface.

## Need examples?

Simply look in to the [examples folder](api/examples) which you can find in `/api/examples`